var app = angular.module("main", ['ngRoute']);

//-------------------------------------------------------------------------
//---------------------PAGE ROUTES & CONTROLLERS---------------------------
//-------------------------------------------------------------------------


app.config(['$routeProvider',function($routeProvider){
    $routeProvider
        .when('/',{
            'templateUrl' : 'partials/home.html',
            // 'controller' :  'mainCtrl'
        })
        .when('/home',{
            'templateUrl' : 'partials/home.html',
        })
        .when('/login',{
            'templateUrl' : 'partials/login.html',
            'controller' : 'loginserviceCtrl'
        })
        .when('/about',{
            'templateUrl' : 'partials/about.html',
            'controller' : 'aboutCtrl'
        })
        .when('/register',{
            'templateUrl' : 'partials/register.html',
            'controller' : 'registerserviceCtrl'
        })
        .when('/contact',{
            'templateUrl' : 'partials/contact.html',
            'controller' : 'contactCtrl'
        })
        .when('/admin',{
            'templateUrl' : 'partials/admin.html',
            'controller' : 'adminpageCtrl'
        })
        .when('/profile',{
            'templateUrl' : 'partials/profile.html',
            'controller' : 'personaldetailCtrl'
        })
        .when('/edit', {
            'templateUrl' : 'partials/edit.html',
             'controller' : 'updatedetailCtrl'
        })
        .when('/editpersonal', {
            'templateUrl' : 'partials/editpersonal.html',
             
        })
        .otherwise({'redirect':'/'}) 
}]);

//-------------------------------------------------------------------------
//------------------------REGISTER CONTROLLER------------------------------
//-------------------------------------------------------------------------


app.controller('registerserviceCtrl', function ($scope, $http, $location) {
    
    // $scope.firstname = null;
    // $scope.lastname = null;
    // console.log("helloo me agya");

    $scope.registerdone = function(){

        // alert("Kyun pareshan hai")
        
        alert("Welcome to register page");
        // console.log($scope.firstname);
        // console.log("Please AJAAA")
        $http.post('http://localhost:1337/register',
        {
            firstname : $scope.firstname,
            lastname : $scope.lastname, 
            email : $scope.email,
            password : $scope.password,
            dob : $scope.dob, 
            role : $scope.role 
        }).then(function (err,response) {
        
            if(err) 
                throw(err);
            // console.log(data);
            response.json(data);
            alert('Thankyou for registering');
            
            $location.path('/login');
        
    });
}
});

//-------------------------------------------------------------------------
//-------------------------LOGIN CONTROLLER--------------------------------
//-------------------------------------------------------------------------


app.controller('loginserviceCtrl', function ($scope, $http, $rootScope, $location) {
    
    // window.location.reload('/login');
    $scope.email = null;
    $scope.password = null;

    $scope.logindone = function(){
        
        alert("You are now logged IN");
        console.log($scope);
        
        console.log($scope.email);
        $http.post('http://localhost:1337/login',
        { 
            email : $scope.email,
            password : $scope.password
        
        }).then(function (response) {
                console.log(response.data);
                
                $scope.email = response.data.email;
                //$scope.password = response.data[0].password;
                console.log($scope.email);
                $scope.role= response.data.role;
                console.log($scope.role);
                
                if($scope.role == 'admin'){
                    localStorage.setItem('email', response.data.email);
                    // localStorage.setItem('flare', true);   
                    $rootScope.flare=true;
                    $location.path('/admin');
                }
                else if($scope.role == 'normal'){
                    localStorage.setItem('email', response.data.email);
                    $rootScope.flare=true;
                    $location.path('/profile')
                }
        },function(response){
            alert("Email not found");

            })
    };

});
//-------------------------------------------------------------------------
//-------------------------ADMIN PAGE CONTROLLER---------------------------
//-------------------------------------------------------------------------


app.controller('adminpageCtrl', function ($scope, $http){

    // $scope.showdetails = function(){
        
        $http.post('http://localhost:1337/fetch', {
            firstname : $scope.firstname,
            lastname : $scope.lastname, 
            email : $scope.email,
            //password : $scope.password,
            role : $scope.role, 
            dob : $scope.dob, 
        }).then(function(response){
            $scope.details = response.data;
            console.log($scope.details);
        })
    
});

//-------------------------------------------------------------------------
//----------------------PERSONAL DETAIL CONTROLLER-------------------------
//-------------------------------------------------------------------------

app.controller('personaldetailCtrl', function ($scope, $http, $rootScope, $location){

        
        $http.post('http://localhost:1337/fetchsingleuser', {
           
            email : localStorage.getItem('email'),
            
        }).then(function(res){
            $scope.firstname = res.data.firstname;
           
            $scope.lastname = res.data.lastname;
            $scope.role = res.data.role;
            $scope.dob = res.data.dob;
            $scope.id = res.data.id;
            $scope.email = res.data.email;
            $scope.pas = res.data.email;
        }),

        $scope.editprofiledetails = function ( firstname, lastname,email, role, dob, password) {
            $rootScope.gfirstname = firstname;
            $rootScope.glastname = lastname;
            $rootScope.gemail = email;
            $rootScope.grole = role;
            $rootScope.gdob = dob;
            $rootScope.gpassword = password;
            $location.path("/editpersonal");
          };

    
});

//-------------------------------------------------------------------------
//------------------------UPDATE DETAILS CONTROLLER------------------------
//-------------------------------------------------------------------------


app.controller('updatedetailCtrl', function ($scope, $http, $location, $rootScope){

    console.log(localStorage);
    console.log($scope.editdata);
    if(localStorage.getItem('email') == $scope.editdata )
    {
        $scope.flag = true;
        console.log($scope.flag);
    }
    else{
        $scope.flag = false;
    }
    // if($scope.password==" ")
    console.log($rootScope.editbirth);
    console.log($rootScope.editroles);
    console.log($rootScope.editlname);
     $scope.updatedetails = function(){
        //  alert('Your details are updated');
        console.log($rootScope.editfname);
        console.log($scope.editlname);
        $http.post('http://localhost:1337/update', {
            firstname : $scope.editfname,
            lastname : $scope.editlname, 
            email : $scope.editdata,
            password : $scope.editpassword,
            role : $scope.editroles,
            dob : $scope.editbirth,  
        }).then(function(response){
            console.log("Value of response.data");
            console.log(response.data);
            $scope.data= response.data;
            console.log($scope.data);
            alert('Updated successfully');
            $location.path('/admin');
        })
    },
    $scope.updatepersonaldetails = function(){
        alert('Edited personal details');
        console.log($scope.gemail);

        $http.post('http://localhost:1337/update', {
            firstname : $scope.gfirstname,
            lastname : $scope.glastname, 
            email : $scope.gemail,
            password : $scope.gpassword,
            role : $scope.grole, 
            dob : $scope.gdob,
        }).then(function(response){
            $scope.data= response.data;
            console.log($scope.data);
            alert('Personal details updated successfully');
            $location.path('/profile');

    })
    }
    
});



//-------------------------------------------------------------------------
//----------------ADMIN EDIT AND DELETE DETAILS CONTROLLER-----------------
//-------------------------------------------------------------------------

app.controller('editdetailCtrl', function ($scope, $location, $rootScope,$http){


    // $scope.editdetails = function(editfirstname,editlastname,editemail,editpassword,editrole,editdob){
        $scope.editdetails = function(x){
            console.log(x);
        console.log(x.dob);
        $rootScope.editfname = x.firstname;
        $rootScope.editlname = x.lastname;
        $rootScope.editdata = x.email;
        $rootScope.editdpass = x.password;
        $rootScope.editroles = x.role;
        $rootScope.editbirth = x.dob;

        $location.path('/edit');
   },

   $scope.deletedetails = function(deleteid){
    alert('Are you sure you want to delete this?')
    //console.log($scope);
    console.log(deleteid);
    $http.delete('http://localhost:1337/delete/'+ deleteid)
    .then(function(response){
        
        window.location.reload(true);
        console.log(response);
        //$scope.id = response.data[0].id;
        // console.log($scope.id);
    })
}
});


//HOME CONTROLLER
app.controller('aboutCtrl', function(){

});
//CONTACT CONTROLLER
app.controller('contactCtrl', function(){
    $scope.click =function(){
        alert('Thankyou for contacting us');
    }
});

//MAIN APP CONTROLLER
app.controller('mainCtrl', ['$scope', '$rootScope','$location', function($scope,$rootScope,$location){
    // $scope.message = 'Everyone come and see how good I look!';

    // if(localStorage.getItem('flare')){
        
    //     $scope.flare=true;
        // $location.path('/admin');
    // }
    // else{
        //$scope.flare=false;
    //     alert('Please Login Again');
    // }
    // console.log($scope.flare);


//--------------------------------------------
//--------------LOGOUT FUNCTION---------------
//--------------------------------------------
$scope.logout = function(){
    localStorage.removeItem('email');
    $location.path('/login');
    $rootScope.flare=false;
}

}]);